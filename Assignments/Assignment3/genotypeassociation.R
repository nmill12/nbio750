options(stringsAsFactors = FALSE);

## Remember to move to directory with your files
setwd('~/Neuroanalytics/Homework_3_part2')
pdf('plot.pdf')


### Create variables to have files 
fped <- "hapmap1.ped";
fphe <- "qt.phe";
fmap <- "hapmap1.zip";

## Reads in fped variable that contains hapmap1.ped file
ped <- read.table(fped)

## eliminates first 6 columns since we don't care
## about the ids of people 
## want to assign it to a separate variable
tmpSNPs <- ped[,c(7:ncol(ped))]

## divide number of columns by 2 to get
nSNPs <- ncol(tmpSNPs)/2

ndonors <- nrow(ped)

cat('The total number of SNPs is:',nSNPs,'\n');
cat('The total number of donors is:',ndonors,'\n');

##Make an output matrix
SNPs <- matrix(0,nrow=ndonors,ncol=nSNPs);


for (i in 1:nSNPs) {
  snprecode <- matrix(NA, nrow=ndonors, ncol=1);
  
  onesnp <- tmpSNPs[,c(2*i-1,2*i)];
  
  ##Classify each SNP into 5 categories
  NAind <- which(onesnp[,1]==0 & onesnp[,2]==0);
  homlind <- which(onesnp[,1]==1 & onesnp[,2]==1);
  hetind <- which((onesnp[,1]==1 & onesnp[,2]==2) | (onesnp[,1]==2 & onesnp[,2]==1));
  hom2ind <- which(onesnp[,1]==2 & onesnp[,2]==2);
  
  
  snprecode[NAind] <- NA;
  snprecode[homlind] <- 0;
  snprecode[hetind] <- 1;
  snprecode[hom2ind] <- 2;
  
  SNPs[,1] <- snprecode;
}

map <- read.table(fmap);

colnames(SNPs) <- map$V2;

rownames(SNPs) <- ped[,1];

phe <- read.table(fphe);


for (z in 1:100) {
  plot(SNPs[,z],phe$V3);

}

dev(off)