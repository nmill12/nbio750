options(stringsAsFactors=FALSE);
library(tidyverse)

#set wd
setwd("~/NBIO_assignments/Assignments/Assignment5")

#define variables
flowafmap <- "hapmap1_nomissing_AF.map";
flowafped <- "hapmap1_nomissing_AF.ped";

#filter by SNPs with AF between 0-0.5?


#define variables
fmap <- "hapmap1.map";
fphe <- "qt.phe"
fped <- "hapmap1.ped";
fsnp <- "nomissing.txt";
fpedout <- "hapmap1_nomissing.ped";
fmapout <- "hapmap1_nomissing.map";
flowafsnps <- "lowafsnps.txt";
flowafmap <- "hapmap1_nomissing_AF.map";
flowafped <- "hapmap1_nomissing_AF.ped";

#read in files
ped <- read.table(fped);
#filter ped to every 6 & 7th column, map to the 2nd
tmpSNPs <- ped[,7:ncol(ped)]; #remove cols 1-6 (patient info)
nSNPs <- ncol(tmpSNPs)/2;
map <- read.table(fmap);
ndonors <- nrow(ped);

#for every 6th & 7th column, append the genotype from 
#fped and SNP name from fmap to empty matrix

SNPs <- matrix(NA,nrow=ndonors,ncol=nSNPs); #matrix of all snps + donor
for (i in 1:nSNPs) { #find genotype condition from data pair
  recodedSNP <- matrix(NA,nrow=ndonors,ncol=1);
  onesnp <- tmpSNPs[,c(i*2-1,i*2)]; #gives a pair of SNP columns for each loop
  indNA <- which(onesnp[,1]==0 & onesnp[,2]==0);
  indhom1 <- which(onesnp[,1]==1 & onesnp[,2]==1);
  indhet <- which((onesnp[,1]==1 & onesnp[,2]==2) | (onesnp[,1]==2 & onesnp[,2]==1));
  indhom2 <- which(onesnp[,1]==2 & onesnp[,2]==2); #all rows with 2-2 listed
  #combine data pair into single value
  recodedSNP[indNA]=NA;
  recodedSNP[indhom1]=0;
  recodedSNP[indhet]=1;
  recodedSNP[indhom2]=2;
  SNPs[,i]=recodedSNP;
}

#assign row and column names
colnames(SNPs) = map$V2;
rownames(SNPs) = ped[,1];

missrate = colSums(is.na(SNPs))/ndonors;
missingind = which(missrate>0.05);

#save list of missing SNPs
missingrs <- colnames(SNPs)[missingind];


#save .map of nomissing
map.noms<-map[-missingind,];
SNPs.noms <- SNPs[,missingind];
pedSNPs.noms<-matrix(NA,nrow=ndonors,ncol=(2*ncol(SNPs.noms)));
#denote if SNP is NA, hom1, het, or hom2
for (i in 1:ncol(SNPs.noms)) {
  NAind = which(is.na(SNPs.noms[,i]));
  hom1ind = which(SNPs.noms[,i]==0);
  hetind = which(SNPs.noms[,i]==1);
  hom2ind <- which(SNPs.noms[,i]==2);
  
  newpedind1 = i*2-1;
  newpedind2 = i*2;
  #assign allele number
  pedSNPs.noms[NAind,newpedind1] = 0;
  pedSNPs.noms[NAind,newpedind2] = 0;
  pedSNPs.noms[hom1ind,newpedind1] = 1;
  pedSNPs.noms[hom1ind,newpedind2] = 1;
  pedSNPs.noms[hetind,newpedind1] = 1;
  pedSNPs.noms[hetind,newpedind2] = 2;
  pedSNPs.noms[hom2ind,newpedind1] = 2;
}

#bind to matrix with original ped 1:6 columns
ped.noms = cbind(ped[,1:6],pedSNPs.noms);

#save nomissing .ped
#sum all columns not missing
nomissingdonors <- colSums(!is.na(SNPs.noms));


#find allele frequency
AF <- colSums(SNPs.noms, na.rm=TRUE)/(nomissingdonors*2);


#filter by >95% or <5%
lowafind = which(AF>0.95 | AF<0.05);
lowafsnps = colnames(SNPs.noms)[lowafind];

AF_minorfreq = matrix(NA, nrow=length(AF), ncol=1);
rownames(AF_minorfreq) = rownames(AF);
for (i in 1:length(AF)) {
  if (AF[i]>0.5) {
    AF_minorfreq[i] = 1-AF[i];
  } else {
    AF_minorfreq[i] = AF[i]
  }
}

pdf('~/NBIO750_assignments/Assignments/Assignment5/geno_histo.pdf')

ggplot(data=as.data.frame(AF_minorfreq), aes(x=AF_minorfreq[,1])) + 
  geom_histogram(color='dark blue', fill='light blue',alpha=0.5) + labs(x="Minor Allele Frequencies", y= "Count",title="Minor Allele Freq Count")

dev.off()