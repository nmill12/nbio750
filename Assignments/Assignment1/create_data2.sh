#!/bin/bash

#Create a series of files in a file directory 
#Called Donor_Files

#this will make a directory to store the files
mkdir donordir;


# this for loop creates the 50 donor files
for i in `seq 1 50`
do
    for j in `seq 1 10` ## nested looop to create donor files with the ten time points
    do
	echo "Creating file: file${i}_tp${j}.txt"; 
	touch donordir/donor${i}_tp${j}.txt;
	echo "data" >> donordir/donor${i}_tp${j}.txt ## this creates header
	echo $RANDOM >> donordir/donor${i}_tp${j}.txt ## this and the copies below create the random data points
	echo $RANDOM >> donordir/donor${i}_tp${j}.txt
	echo $RANDOM >> donordir/donor${i}_tp${j}.txt
	echo $RANDOM >> donordir/donor${i}_tp${j}.txt
	echo $RANDOM >> donordir/donor${i}_tp${j}.txt; 
    done

done

pattern='%02d' ##

for i in 'seq 1 50'
do
    for j in 'seq 1 10'
    do

	echo "Renaming file: donor${i}_tp${j}";
	if [ -f donor${i}_tp${j} ]; then
	       

