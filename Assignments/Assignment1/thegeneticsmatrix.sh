#!/bin/bash

pathname="/nas/longleaf/home/mariamag/hapmap1.ped"
FILE=$(basename "$pathname")
echo $FILE

#This assigns Variable 1 (VAR1) to number of lines in hapmap1.ped. WC is word count. -l is line number. < hapmap1.ped will set the number of lines and not the file name into the variable
VAR1=`wc -l < $FILE`

## This for loop will go through the number of lines, which is 89 lines
for i in `seq 1 $VAR1`
do
    tail -n $i $FILE;#This will use the tail -n command will output lines as the program is going through the for loop. 
    sleep 1.0;
done
