options(stringsAsFactors = FALSE)


## Loops for job submission
for (i in 1:100) {
  
  system(paste0("sbatch -p general -N 1 --mem=4g -n 1 -t 00:20:00 -o output",i, ".out --wrap=\"Rscript perm_test_1.R ",i,"\""));

}