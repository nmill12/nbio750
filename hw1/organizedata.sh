#!/bin/bash

mkdir fakedata/ #will make directory named fakedata
for x in `seq 1 50` #will sequence through 50 times
do
    mkdir donor${x}_files/
#will make directory for each donor, titled donor`1-50`_files
    for y in `seq 1 10` #will sequence through 10 times
    do
	chmod a-wx donor${x}_tp${y}.txt 
	mv donor${x}_tp${y}.txt donor${x}_files/
#will move donor_tp files to donor`1-50`_files directory
    done
    mv donor${x}_files/ fakedata/
done
