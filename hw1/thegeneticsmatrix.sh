#!/bin/bash

FILE=$(basename hapmap1.ped) 
#assigns hapmap1.ped without directory path to FILE. I don't think this is necessary, but it said we should use basename, so I made it work
LENGTH=$(<$FILE wc -l) 
#wc -l outputs the number of lines in $FILE (hapmap1.ped)
for i in `seq 1 $LENGTH`
#sequences through 1 and the total # lines, so each line will be referenced
do
tail -${i} hapmap1.ped | cat
#takes each line and displays it with the cat command. I tried to get this to work with more and less, but they didn't flow like a matrix like cat or awk did
sleep 1;
#1 second delay between next i value, reading next line.
#of note, it will present all the content of each line at once, which is more than can be displayed on a single line, so it looks like big chunks of data are being presented at once. I would rather it put a delay between each line displayed as it is being read out, but that was a little more complicated
done
