#!/bin/bash

for x in `seq 1 50` #will sequence through 50 times
do
    for y in `seq 1 10` #will sequence through 10 times
    do
	echo -e "data\r\n$RANDOM\r\n$RANDOM\r\n$RANDOM\r\n$RANDOM\r\n$RANDOM" | tee -a donor${x}_tp${y}.txt
#makes 10 tp files with a data header and 5 random numbers in separate lines (due to -e and \n) for each of 50 donors. | tee -a appends to the donor${x}_tp${y}.txt file. Should be able to also do >> donor${x}_tp${y}.txt
    done
done
