#!/bin/bash

## -n stops trailing new line; -e adds enable interpretation fo backslash escapes; \b functions as a backspace; \r carriage return:returns cursor to new line##
#original swirl

echo -ne '/';
sleep 1; #1 second delay
echo -ne '\b-'; 
sleep 1;
echo -ne '\b\\'
sleep 1;
echo -ne '\b|';
sleep 1;
echo -ne '\b/';
sleep 1;
echo -ne '\b-';
sleep 1;
echo -ne '\b\\';
sleep 1;
echo -ne '\b|';
echo -ne "\b \n";
#faster original swirl
echo -ne '/';
sleep 0.2; #0.2 second delay
echo -ne '\b-';
sleep 0.2;
echo -ne '\b\\'
sleep 0.2;
echo -ne '\b|';
sleep 0.2;
echo -ne '\b/';
sleep 0.2;
echo -ne '\b-';
sleep 0.2;
echo -ne '\b\\';
sleep 0.2;
echo -ne '\b|';
echo -ne "\b \n";
#faster, reverse swirl 
echo -ne '\'; #switched direction of slashes
sleep 0.2;
echo -ne '\b-';
sleep 0.2;
echo -ne '\b/';
sleep 0.2;
echo -ne '\b|';
sleep 0.2;
echo -ne '\b\';
sleep 0.2;
echo -ne '\b-';
sleep 0.2;
echo -ne '\b/';
sleep 0.2;
echo -ne '\b|';
sleep 0.2;
echo -ne '\b\';
sleep 0.2;
echo -ne '\b';
#10 sequence of spinning
for i in `seq 1 10` #$i will sequence from 1-10; will make 10 swirls
do
  echo -e "\rstarting swirl " ${i};
  echo -ne '\';
  sleep 0.1;
  echo -ne '\b-';
  sleep 0.1;
  echo -ne '\b/';
  sleep 0.1;
  echo -ne '\b|';
  sleep 0.1;
  echo -ne '\b\';
  sleep 0.1;
  echo -ne '\b-';
  sleep 0.1;
  echo -ne '\b/';
  sleep 0.1;
  echo -ne '\b|';
  sleep 0.1;
  echo -ne '\b\';
  sleep 0.1;
  echo -ne '\b';
done
#10 sequence of action via for loop
for i in `seq 1 10`
do
   echo -e "starting dance " ${i};
   if ((${i} % 2))
   then
       echo -e ' o';
       echo -e '/|\';
       echo -e ' O';
       echo -e '\e[1A\e[k /|'; 
#\e[1A\e[k uses \e[1A to return to the previous line and \e[k to clear that line, so that each line clears the previous echo result
       sleep 0.4;
       echo -e '\e[1A\e[k |\';
       sleep 0.4;
       echo -e '\e[1A\e[k /|';
       sleep 0.4;
       echo -e '\e[1A\e[k |\';
       sleep 0.4;
       echo -e '\e[1A\e[k /|';
       sleep 0.4;
       echo -e '\e[1A\e[k |\';
       sleep 0.4;
   else 
       echo -e ' o';
       echo -e '\|/';
       echo -e ' O';
       echo -e '\e[1A\e[k |\';
       sleep 0.4;
       echo -e '\e[1A\e[k /|';
       sleep 0.4;
       echo -e '\e[1A\e[k |\';
       sleep 0.4;
       echo -e '\e[1A\e[k /|';
       sleep 0.4;
       echo -e '\e[1A\e[k |\';
       sleep 0.4;
       echo -e '\e[1A\e[k /|';
   fi
done
