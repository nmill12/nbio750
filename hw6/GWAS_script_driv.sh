options(stringsAsFactors = FALSE)


## Loops for job submission
for (i in 1:1000) {
  
  system(paste0("sbatch -p general -N 1 --mem=4g -n 1 -t 00:20:00 -o output",i, ".out --wrap=\"GWAS_long_script.R ",i,"\""));
  
}