Aside from a large number of SNPs isogenic in the population (AF = 1, shown as 0 here due to 1-AF transformation), allele frequency does not differ greatly across SNPs
