SNP phenotype occurrence by severity is normally distributed, with very mild and very severe phenotypes occurring less often than intermediate phenotypes with a severity rating between 5-8
