options(stringsAsFactors=FALSE);
library(tidyverse)
library(stringr) #in order to use str_split function

## set wd
setwd("C:/Users/nmill/Desktop/NBIO750");

## Set input names
## read file using read_tsv
freqetoh <- read_tsv('20414.gwas.imputed_v3.both_sexes.tsv')

## extracting out chromosome and p-value into own matrix
chrom <- freqetoh$variant
pval <- as.numeric(freqetoh$pval)
sigpval <- data.frame(cbind(chrom,pval))

## transforms pval to numeric instead of chr
sigdf <- transform(sigpval, pval = as.numeric(pval))

## filter out the SNPS that contains a p value less than >1x10e-4
strgsnps <- filter(sigdf, pval < 0.0001)

## split chrom using str_split_fixed function
strgsnps2 <- as.data.frame(str_split_fixed(strgsnps$chrom, ":", 5))

## to add pval to the 5th column
strgsnps2[,5] <- strgsnps$pval


## renaming column names
colnames(strgsnps2) <- c('chrom', 'pos', 'allele1', 'allele2', 'pval')

## filter again to have df with just chromosome 4
chrom4 <- filter(strgsnps2, chrom == 4)
chrom4_num<-transform(chrom4, pos=as.numeric(pos))
## now will use ggplot2 to plot
pdf('AlcoholManhattan.pdf')

ggplot(chrom4_num) + geom_point(mapping=aes(x=pos, y=pval)) + scale_y_log10()

dev.off()
