#!/bin/bash

for x in {001..100} #will sequence through 100 times with leading zeros
do
    for y in {001..010} #will sequence through 10 times with leading zeros
    do
	echo -e "data\r\n$RANDOM\r\n$RANDOM\r\n$RANDOM\r\n$RANDOM\r\n$RANDOM" | tee -a donor${x}_tp${y}.txt
#makes 10 tp files with a data header and 5 random numbers in separate lines (due to -e and \n) for each of 50 donors. | tee -a appends to the donor${x}_tp${y}.txt file. Should be able to also do >> donor${x}_tp${y}.txt
    done
done
